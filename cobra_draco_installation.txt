This is how you do it on cobra/draco (assuming you are using intel compiler):
1) git clone git@aims-git.rz-berlin.mpg.de:aims/FHIaims.git
2) cd FHIaims; mkdir build; cd build
3) Copy the attached file into that folder
4) cmake -C cobra_draco.cmake ..
(Don’t forget the two dots! And use capital C)
5) make -j 8
